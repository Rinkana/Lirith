# Lirith

Lirith is a PHP framework with the primary goal to be logical for the programmer.

Many frameworks today make the use of too many "tools" that, in the end, makes the development of websites even harder.
So what i want to create is a framework that has everything a framework needs in its core.
* It does not depend on things like composer for extra dependencies.
* It does not use tools like Artisan to handle tasks that your ide uses.


* It is made to be friendly with your IDE, so no odd references and no need for an ide-helper file.
* It is made so that the developer can easily find what the framework actually does.

### Version
0.0.1

### Installation
1. Clone this repo
2. Point your webserver to the `public` folder
3. Go to your website