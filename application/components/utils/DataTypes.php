<?php
/**
 * Created by IntelliJ IDEA.
 * User: max
 * Date: 06-Feb-16
 * Time: 15:39
 */

namespace utils;


class DataTypes
{
    const INCREMENTS = "increments";

    const INTEGER = "integer";
    const LONG = "long";
    const SHORT = "short";
    const FLOAT = "float";
    const DOUBLE = "double";
    const BOOLEAN = "boolean";
    const STRING = "string";
    const TEXT = "text";

    const TIMESTAMP = "timestamp";
    const DATE = "date";
    const TIME = "time";
}